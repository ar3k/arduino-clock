#include <SoftwareSerial.h>
#include <Wire.h>
#include "RTClib.h"

RTC_DS1307 RTC; // SLC in A5 SDA in A4
SoftwareSerial BTSerial(8,9); //make a serial connection between arduino and HC-05
//RX,TX a2, a3 

const int SER = 5; //si 14
const int SRCLK = 7; //sck 11
const int RCLK =  6; //rck 12

const int numberOfPins = 8;
int myRegister[numberOfPins];

const int d4 = 2;
const int d3 = 3;
const int d2 = 4;
const int d1 = 12;

const int buz = 15;//a1
int x = 20;
float y = 0;

const int btn = 13;
int btnState = 0;
int stopHour=-1,stopMin = -1; 

const int red = 17;
const int green = 16;
const int blue = 14;

int dataInt=0;
//for saving alarm
String alarmString= "";
int alarmHour, alarmMinute;
bool shouldAlarmRing = false;

bool shouldBeTimeSet = false;

bool isNotification = false;
bool isLedOn = false;
int startSec = -1;
int stopSec = -1;

int redRGB;
int greenRGB;
int blueRGB;

int hour;
int minute;
int sec;

void setup() {
  Serial.begin(9600);
  Serial.println("??????????????????????");
  pinMode(13, OUTPUT);
  digitalWrite(13, LOW);
  
  pinMode(d1, OUTPUT);
  pinMode(d2, OUTPUT);
  pinMode(d3, OUTPUT);
  pinMode(d4, OUTPUT);

  pinMode(SER, OUTPUT);
  pinMode(RCLK, OUTPUT);
  pinMode(SRCLK, OUTPUT);
  
  pinMode(buz, OUTPUT);

  pinMode(btn, INPUT);

  pinMode(red, OUTPUT);
  pinMode(green, OUTPUT);
  pinMode(blue, OUTPUT);
  
  Wire.begin();
  RTC.begin();
  BTSerial.begin(9600);

  cleanRegister();
  saveRegister();
}

void loop() {
  //Serial.println("!!!!!!!!!!!!!");
  setTimeVariablesWithRTC();
  
  checkIsAlarmNeededAndRingIfNeeded();
  checkIfIsNotificationAndBlinkLed();
  
  if(BTSerial.available() >0){
    doThisIfSomeBytesGetIn();
  }

  displayTime();
}

void checkIfIsNotificationAndBlinkLed(){
  if(isNotification && !isLedOn){
    Serial.print(redRGB),Serial.print(greenRGB), Serial.println(blueRGB);
    blinkRGBWithColorsGlobalVariables();
    startSec = sec;
    if(startSec>=45){
      stopSec = startSec - 45;
    }else{
      stopSec = startSec + 15;
    }
    isLedOn = true;
    Serial.println(startSec);Serial.println(stopSec);
  }else if(isNotification && isLedOn){
    if(stopSec == sec){
      Serial.println(sec);
      analogWrite(blue, 0);
      analogWrite(red, 0);
      analogWrite(green, 0);
      isLedOn = false;  
      isNotification = false;
    }
  }
}

void checkIsAlarmNeededAndRingIfNeeded(){

  if(hour == stopHour && minute == (stopMin+1)){
    stopHour = -1;
    stopMin = -1;
  }
  
  if(hour == alarmHour && hour != stopHour){
    if(minute == alarmMinute && minute != stopMin){
      shouldAlarmRing = true;
    }else{
      shouldAlarmRing = false;
    }
  }else{
    shouldAlarmRing = false;
  }

  btnState = digitalRead(btn);
  if(btnState == HIGH){
    shouldAlarmRing = false;
    stopHour = hour;
    stopMin = minute;
  }
  
  if(shouldAlarmRing){
    tone(buz,x);
    x+=1;
    /*while(y<255){
      y+=0.1;
      Serial.println(y);  
    }
    analogWrite(blue,(int)y);*/
  }else{
    noTone(buz);
    x=20;
    /*if(!isNotification){
      analogWrite(blue,0); 
    }*/
  }
}

void displayTime(){
  displayNumberOnOneSegment(d1, hour/10);
  displayNumberOnOneSegment(d2, hour%10);
  displayNumberOnOneSegment(d3, minute/10);
  displayNumberOnOneSegment(d4, minute%10);
}

void displayNumberOnOneSegment(int numberOfSegment, int numberToDisplay){
 
    int timeOfDelay = 5;
  
    clear();
    switchOffAllSegments();
    digitalWrite(numberOfSegment, HIGH);

    switch(numberToDisplay){
      case 0: zero(); break;
      case 1: one(); break;
      case 2: two(); break;
      case 3: three(); break;
      case 4: four(); break;
      case 5: five(); break;
      case 6: six(); break;
      case 7: seven(); break;
      case 8: eight(); break;
      case 9: nine(); break;
      default: error();
    }
    saveRegister();
    delay(timeOfDelay);
}

void switchOffAllSegments(){
  digitalWrite(d1, LOW);
  digitalWrite(d2, LOW);
  digitalWrite(d3, LOW);
  digitalWrite(d4, LOW);
}

void doThisIfSomeBytesGetIn(){
    dataInt = BTSerial.read();
    //Serial.println((char)dataInt);
    switch((char)dataInt){
      case 'a': 
        aIsTheFirstChar();
        //Serial.println(alarmString);
        //setTimeVariablesWithRTC();
        //Serial.print(alarmHour);Serial.println(alarmMinute);
        break;
      case 's':
        sIsTheFirstChar();
        break;
      case 'n':
        nIsTheFirstChar();
        break;
    }
}


void aIsTheFirstChar(){
  int len = 4; 
  int i;
  alarmString = "";
  i=1;
  while(i<=len){
    dataInt = BTSerial.read();
    if(dataInt!=-1){
      alarmString+=(char)dataInt;
      i++;
    }
    String hourTmp="";
    hourTmp+=alarmString[0];
    hourTmp+=alarmString[1];
    String minTmp = "";
    minTmp+=alarmString[2];
    minTmp+=alarmString[3];
    alarmHour = hourTmp.toInt();
    alarmMinute = minTmp.toInt();
    //Serial.println(dataInt);
  }
  
  stopHour = -1;
  stopMin = -1;
}

void sIsTheFirstChar(){
  int len = 4; 
  int i;
  String timeString= "";
  //Serial.println((char)dataInt);
  for(i = 1;i<=len;i++){
     dataInt = BTSerial.read();
     timeString+=(char)dataInt;
    // Serial.println(dataInt);
  }
  //Serial.println(timeString);
  String hourString = "";
  hourString+=timeString[0];
  hourString+=timeString[1];
  String minuteString = "";
  minuteString+=timeString[2];
  minuteString+=timeString[3];
  setRTCTime(hourString.toInt(), minuteString.toInt());
}

void nIsTheFirstChar(){
 String notificationPackage="";
  while(BTSerial.available() >0){
    dataInt = BTSerial.read();
    notificationPackage+=(char)dataInt;
  }
  Serial.println(notificationPackage);
  char packageCharacter = notificationPackage[0];
  Serial.println(packageCharacter);
  setRGBColorsDependingOnApp(packageCharacter);
  isNotification = true;
}

void setRGBColorsDependingOnApp(char app){
  redRGB = 0;
  greenRGB = 0;
  blueRGB = 0;
  switch(app){
    case 'w':
      redRGB = 255;
      greenRGB = 102;
      blueRGB = 102;
      break;
    case 'f':
      redRGB = 0;
      greenRGB = 102;
      blueRGB = 204;
      break;
    case 's':
      redRGB = 0;
      greenRGB = 255;
      blueRGB = 128;
      break;
  }
}

void blinkRGBWithColorsGlobalVariables(){
  analogWrite(blue, blueRGB);
  analogWrite(red, redRGB);
  analogWrite(green, greenRGB);
}

void setTimeVariablesWithRTC(){
    DateTime now = RTC.now(); 
    hour = now.hour();
    minute = now.minute();
    sec = now.second();
//    Serial.print(hour);Serial.print(" ");Serial.println(minute); 
}

void setRTCTime(int h, int m){
  RTC.adjust(DateTime(2018, 1, 1, h, m, 00));
  //Serial.println("Set RTC time to: "+h+m);
}

void zero() //the 7-segment led display 0
{
  int tmpRegister[] = {1,0,0,0,0,0,0,1};
  setRegister(tmpRegister);
}

void one(){
  int tmpRegister[] = {1,1,0,0,1,1,1,1};
  setRegister(tmpRegister);
}

void two(){
  int tmpRegister[] = {1,0,0,1,0,0,1,0};
  setRegister(tmpRegister);
}

void three(){
  int tmpRegister[] = {1,0,0,0,0,1,1,0};
  setRegister(tmpRegister);
}

void four(){
  int tmpRegister[] = {1,1,0,0,1,1,0,0};
  setRegister(tmpRegister);
}

void five(){
  int tmpRegister[] = {1,0,1,0,0,1,0,0};
  setRegister(tmpRegister);
}

void six(){
  int tmpRegister[] = {1,0,1,0,0,0,0,0};
  setRegister(tmpRegister);
}

void seven(){
  int tmpRegister[] = {1,0,0,0,1,1,1,1};
  setRegister(tmpRegister);
}

void eight(){
  int tmpRegister[] = {1,0,0,0,0,0,0,0};
  setRegister(tmpRegister);
}

void nine(){
  int tmpRegister[] = {1,0,0,0,0,1,0,0};
  setRegister(tmpRegister);
}

void error(){
  int tmpRegister[] = {1,0,1,1,0,0,0,0};
  setRegister(tmpRegister);
}

void clear(){
  cleanRegister();
  saveRegister();
}

void saveRegister(){
  digitalWrite(RCLK, LOW); 
  for(int i=numberOfPins-1; i>=0; i--){
    digitalWrite(SRCLK, LOW);
    digitalWrite(SER, myRegister[i]);
    digitalWrite(SRCLK, HIGH); 
  }
  digitalWrite(RCLK, HIGH); 
}

void cleanRegister(){
  for(int i=0; i<numberOfPins; i++)
    myRegister[i]=HIGH;
}

void setRegister(int tmpRegister[]){
  for(int i=0;i<numberOfPins;i++){
    myRegister[i]=tmpRegister[i];
  }
}
